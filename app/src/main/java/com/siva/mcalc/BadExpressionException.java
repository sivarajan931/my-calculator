package com.siva.mcalc;

public class BadExpressionException extends Exception {
    BadExpressionException(String str){
        super(str);
    }
}
