package com.siva.mcalc;

import java.util.Vector;
import java.util.Iterator;
public class calc {
    Vector<Double> nu;
    Vector<Character>op;
    double result;
    calc(int n){
        nu = new Vector<>(n);
        op = new Vector<>(n);
    }
    private boolean isOperator(char x){
        return (x =='+' || x =='-' || x =='*'  || x =='/');
    }
    void parse(String expression){
        String a = expression.replace("+"," ").replace("/"," ")
                .replace("-"," ").replace("*"," ");
        String[] num_list = a.split(" ");
        for (String s : num_list) nu.add(Double.valueOf(s));
        StringBuilder b = new StringBuilder(expression);
        for(int i =0;i<b.length();++i){
            if(isOperator(b.charAt(i)))
                op.add(b.charAt(i));
        }
    }
    void evaluate(){
        Iterator<Double> numbers = nu.iterator();
        Iterator<Character> operator = op.iterator();
        char p ;
        double res=numbers.next();
        while(operator.hasNext() && numbers.hasNext()){
            p = operator.next();
            double x = numbers.next();
            if(p == '+')  res+=x;
            else if(p == '-')  res-=x;
            else if(p == '*')  res*=x;
            else if(p == '/')  res/=x;
        }
        this.result=res;
    }

}
