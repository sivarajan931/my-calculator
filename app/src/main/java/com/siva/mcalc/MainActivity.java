package com.siva.mcalc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EditText et1 = findViewById(R.id.editTextTextPersonName);
        Button[] b= new Button[10];


        findViewById(R.id.btnadd).setOnClickListener(view->et1.append("+"));
        findViewById(R.id.btnSub).setOnClickListener(view->et1.append("-"));
        findViewById(R.id.btnmul).setOnClickListener(view->et1.append("*"));
        findViewById(R.id.btndiv).setOnClickListener(view->et1.append("/"));
        findViewById(R.id.BtnClr).setOnClickListener(view->et1.setText(""));
        findViewById(R.id.ev).setOnClickListener(view->{
            try {
                final calc c = new calc(100);
                c.parse(String.valueOf(et1.getText()));
                c.evaluate();
                et1.setText(String.valueOf(c.result));
            }catch(Exception e){
                Toast.makeText(this,"Error",Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        });
        b[0] = findViewById(R.id.button11);
        b[1] = findViewById(R.id.button);
        b[2] = findViewById(R.id.button2);
        b[3] = findViewById(R.id.button3);
        b[4] = findViewById(R.id.button4);
        b[5] = findViewById(R.id.button5);
        b[6] = findViewById(R.id.button6);
        b[7] = findViewById(R.id.button7);
        b[8] = findViewById(R.id.button8);
        b[9] = findViewById(R.id.button9);
        for(int i =0; i < b.length; ++i){
            int finalI = i;
            b[i].setOnClickListener(view -> et1.append(Integer.toString(finalI)));
        }
    }
}

